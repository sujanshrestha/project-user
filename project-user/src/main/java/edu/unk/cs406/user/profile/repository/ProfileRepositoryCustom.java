package edu.unk.cs406.user.profile.repository;

import edu.unk.cs406.user.entity.UserEntity;

public interface ProfileRepositoryCustom {
	public UserEntity updateUserEntity(UserEntity user);
}
